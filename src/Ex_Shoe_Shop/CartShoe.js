import React, { Component } from 'react'

export default class extends Component {
    renderCartShoe = () => {
        return this.props.cartShoe.map((item) => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    {/* style inline là dùng obj . VD: style={{ width: "50px" }} */}
                    <td><img style={{ width: "50px" }} src={item.image} alt="" /></td>
                    <td>
                        <button className='btn btn-primary' onClick={() => this.props.updateQty(-1, item.id)}>-</button>
                        <span className='mx-3'>{item.qty}</span>
                        <button className='btn btn-danger' onClick={() => this.props.updateQty(1, item.id)}>+</button>
                    </td>
                    <td>{item.price * item.qty}$</td>
                </tr>
            )
        })
    };

    render() {
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Img</td>
                        <td>Qty</td>
                        <td>Price</td>
                    </tr>
                </thead>
                <tbody>
                    {this.renderCartShoe()}
                </tbody>
            </table>
        )
    }
}
