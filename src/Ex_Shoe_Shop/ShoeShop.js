import React, { Component } from 'react'
import CartShoe from './CartShoe';
import { dataShoe } from './dataShoe'
import DetailShoe from './DetailShoe';
import ListShoe from './ListShoe'

export default class ShoeShop extends Component {
    state = {
        shoeArr: dataShoe,
        detailShoe: dataShoe,
        cartShoe: [],
    };

    handleChangeDetailShoe = (shoe) => {
        // truyền tham số shoe vào để biết là đôi giầy mình chọn
        this.setState({
            detailShoe: shoe,
        })
    };

    handleAddToCart = (shoe) => {
        // tạo array mới ~ spread operater
        let cloneCart = [...this.state.cartShoe]
        // TH1 : chưa có sp thì push sp vào giỏ hàng 
        // TH2 : Nếu có sp trong giỏ hàng thì updated Qty
        let index = this.state.cartShoe.findIndex((item) => item.id == shoe.id)
        if (index == -1) {
            // dùng spread operater tạo mới obj dựa trên obj cũ
            let cartItem = { ...shoe, qty: 1 };
            cloneCart.push(cartItem);
        } else {
            cloneCart[index].qty++;
        }
        // cloneCart.push(shoe)
        this.setState({ cartShoe: cloneCart })
    };

    updateQty = (qty, productId) => {
        console.log(qty, productId);
        let cloneCart = [...this.state.cartShoe]
        let index = this.state.cartShoe.findIndex((item) => item.id == productId)
        if (index == -1) {
            return
        }

        const newQty = cloneCart[index].qty + qty
        if (newQty == 0) {
            // xoa sp
            cloneCart.splice(index, 1)
            this.setState({ cartShoe: cloneCart })
            return
        }
        cloneCart[index].qty = newQty;
        this.setState({ cartShoe: cloneCart })
    }


    render() {
        // in ra table cho dể nhìn
        console.table(this.state.shoeArr);
        return (
            <div className='container py-5'>
                <CartShoe updateQty={this.updateQty} cartShoe={this.state.cartShoe} />
                <ListShoe handleAddToCart={this.handleAddToCart} handleChangeDetailShoe={this.handleChangeDetailShoe} shoeArr={this.state.shoeArr} />
                <DetailShoe detail={this.state.detailShoe} />
            </div>
        )
    };
};
