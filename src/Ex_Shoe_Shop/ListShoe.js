import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    renderListShoe = () => {
        // map ~ duyệt mảng , dùng return .map tạo ra array
        return this.props.shoeArr.map((item, index) => {
            return (<ItemShoe handleClick={this.props.handleChangeDetailShoe}
                handleBuyClick={this.props.handleAddToCart} data={item} key={index} />);
        })
    }
    render() {
        return (
            <div className='row'>{this.renderListShoe()}</div>
        )
    }
}
