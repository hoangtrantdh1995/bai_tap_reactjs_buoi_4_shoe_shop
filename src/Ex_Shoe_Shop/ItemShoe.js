import React, { Component } from 'react'

export default class ItemShoe extends Component {
    render() {
        let { name, price, image } = this.props.data
        return (
            <div className='col-3 p-3'>
                <div className="card text-left h-100">
                    <img className="card-img-top" src={image} alt />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <h2 className="card-text">{price}$</h2>
                    </div>
                    <div className='text-center'>
                        <button className='btn btn-secondary' onClick={() => { this.props.handleClick(this.props.data) }}>Xem chi tiết</button>
                        <button className='btn btn-danger' onClick={() => { this.props.handleBuyClick(this.props.data) }}>Mua</button>
                    </div>
                </div>
            </div>
        )
    }
}
