import logo from './logo.svg';
import './App.css';
import ShoeShop from './Ex_Shoe_Shop/ShoeShop';
import { dataShoe } from './Ex_Shoe_Shop/dataShoe';

function App() {
  return (
    <div className="App">
      <ShoeShop />
    </div>
  );
}

export default App;
